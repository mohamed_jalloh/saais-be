const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subjects', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    major_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'class_majors',
        key: 'id'
      }
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'subject_categories',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'subjects',
    timestamps: true,
    paranoid: true,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "subjects_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "major_id" },
          { name: "category_id" },
          { name: "name" },
          { name: "deleted_at" },
        ]
      },
      {
        name: "subjects_major_id",
        using: "BTREE",
        fields: [
          { name: "major_id" },
        ]
      },
      {
        name: "subjects_category_id",
        using: "BTREE",
        fields: [
          { name: "category_id" },
        ]
      },
    ]
  });
};
