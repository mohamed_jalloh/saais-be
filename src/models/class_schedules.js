const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('class_schedules', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    class_subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'class_subjects',
        key: 'id'
      }
    },
    day: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    start_time: {
      type: DataTypes.TIME,
      allowNull: false
    },
    finish_time: {
      type: DataTypes.TIME,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'class_schedules',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "class_schedules_class_subject_id",
        using: "BTREE",
        fields: [
          { name: "class_subject_id" },
        ]
      },
      {
        name: "class_schedules_day",
        using: "BTREE",
        fields: [
          { name: "day" },
        ]
      },
    ]
  });
};
