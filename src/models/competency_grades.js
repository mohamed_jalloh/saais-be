const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('competency_grades', {
    grade_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'grades',
        key: 'id'
      }
    },
    competency_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'competencies',
        key: 'id'
      }
    },
    min_score: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'competency_grades',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "competency_grades_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "grade_id" },
          { name: "competency_id" },
        ]
      },
      {
        name: "competency_grades_grade_id",
        using: "BTREE",
        fields: [
          { name: "grade_id" },
        ]
      },
      {
        name: "competency_grades_competency_id",
        using: "BTREE",
        fields: [
          { name: "competency_id" },
        ]
      },
    ]
  });
};
