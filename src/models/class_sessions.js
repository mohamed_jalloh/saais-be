const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('class_sessions', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    class_subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'class_subjects',
        key: 'id'
      }
    },
    teacher_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    lesson_note: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    started_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    finished_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'class_sessions',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "class_sessions_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "class_subject_id" },
          { name: "teacher_id" },
          { name: "started_at" },
        ]
      },
      {
        name: "class_sessions_class_subject_id",
        using: "BTREE",
        fields: [
          { name: "class_subject_id" },
        ]
      },
      {
        name: "class_sessions_teacher_id",
        using: "BTREE",
        fields: [
          { name: "teacher_id" },
        ]
      },
    ]
  });
};
