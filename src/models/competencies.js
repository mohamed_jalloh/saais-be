const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('competencies', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'subjects',
        key: 'id'
      }
    },
    compentency_category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'competency_categories',
        key: 'id'
      }
    },
    code: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'competencies',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "competencies_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "subject_id" },
          { name: "compentency_category_id" },
        ]
      },
      {
        name: "competencies_subject_id",
        using: "BTREE",
        fields: [
          { name: "subject_id" },
        ]
      },
      {
        name: "competencies_compentency_category_id",
        using: "BTREE",
        fields: [
          { name: "compentency_category_id" },
        ]
      },
    ]
  });
};
