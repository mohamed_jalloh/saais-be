var DataTypes = require("sequelize").DataTypes;
var _cities = require("./cities");
var _class_attendances = require("./class_attendances");
var _class_majors = require("./class_majors");
var _class_schedules = require("./class_schedules");
var _class_sessions = require("./class_sessions");
var _class_subjects = require("./class_subjects");
var _classes = require("./classes");
var _competencies = require("./competencies");
var _competency_categories = require("./competency_categories");
var _competency_grades = require("./competency_grades");
var _grades = require("./grades");
var _histories = require("./histories");
var _roles = require("./roles");
var _school_attendances = require("./school_attendances");
var _school_attendances_summaries = require("./school_attendances_summaries");
var _school_bill_payments = require("./school_bill_payments");
var _school_bills = require("./school_bills");
var _school_periods = require("./school_periods");
var _settings = require("./settings");
var _student_classes = require("./student_classes");
var _student_subjects = require("./student_subjects");
var _subject_categories = require("./subject_categories");
var _subject_scores = require("./subject_scores");
var _subjects = require("./subjects");
var _user_details = require("./user_details");
var _user_roles = require("./user_roles");
var _users = require("./users");

function initModels(sequelize) {
  var cities = _cities(sequelize, DataTypes);
  var class_attendances = _class_attendances(sequelize, DataTypes);
  var class_majors = _class_majors(sequelize, DataTypes);
  var class_schedules = _class_schedules(sequelize, DataTypes);
  var class_sessions = _class_sessions(sequelize, DataTypes);
  var class_subjects = _class_subjects(sequelize, DataTypes);
  var classes = _classes(sequelize, DataTypes);
  var competencies = _competencies(sequelize, DataTypes);
  var competency_categories = _competency_categories(sequelize, DataTypes);
  var competency_grades = _competency_grades(sequelize, DataTypes);
  var grades = _grades(sequelize, DataTypes);
  var histories = _histories(sequelize, DataTypes);
  var roles = _roles(sequelize, DataTypes);
  var school_attendances = _school_attendances(sequelize, DataTypes);
  var school_attendances_summaries = _school_attendances_summaries(sequelize, DataTypes);
  var school_bill_payments = _school_bill_payments(sequelize, DataTypes);
  var school_bills = _school_bills(sequelize, DataTypes);
  var school_periods = _school_periods(sequelize, DataTypes);
  var settings = _settings(sequelize, DataTypes);
  var student_classes = _student_classes(sequelize, DataTypes);
  var student_subjects = _student_subjects(sequelize, DataTypes);
  var subject_categories = _subject_categories(sequelize, DataTypes);
  var subject_scores = _subject_scores(sequelize, DataTypes);
  var subjects = _subjects(sequelize, DataTypes);
  var user_details = _user_details(sequelize, DataTypes);
  var user_roles = _user_roles(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);

  classes.belongsToMany(users, { as: 'student_id_users', through: student_classes, foreignKey: "class_id", otherKey: "student_id" });
  roles.belongsToMany(users, { as: 'user_id_users', through: user_roles, foreignKey: "code", otherKey: "user_id" });
  users.belongsToMany(classes, { as: 'class_id_classes', through: student_classes, foreignKey: "student_id", otherKey: "class_id" });
  users.belongsToMany(roles, { as: 'code_roles', through: user_roles, foreignKey: "user_id", otherKey: "code" });
  user_details.belongsTo(cities, { foreignKey: "place_of_birth_id"});
  cities.hasMany(user_details, { foreignKey: "place_of_birth_id"});
  classes.belongsTo(class_majors, { foreignKey: "major_id"});
  class_majors.hasMany(classes, { foreignKey: "major_id"});
  subjects.belongsTo(class_majors, { foreignKey: "major_id"});
  class_majors.hasMany(subjects, { foreignKey: "major_id"});
  class_attendances.belongsTo(class_sessions, { foreignKey: "class_session_id"});
  class_sessions.hasMany(class_attendances, { foreignKey: "class_session_id"});
  class_schedules.belongsTo(class_subjects, { foreignKey: "class_subject_id"});
  class_subjects.hasMany(class_schedules, { foreignKey: "class_subject_id"});
  class_sessions.belongsTo(class_subjects, { foreignKey: "class_subject_id"});
  class_subjects.hasMany(class_sessions, { foreignKey: "class_subject_id"});
  student_subjects.belongsTo(class_subjects, { foreignKey: "class_subject_id"});
  class_subjects.hasMany(student_subjects, { foreignKey: "class_subject_id"});
  class_subjects.belongsTo(classes, { foreignKey: "class_id"});
  classes.hasMany(class_subjects, { foreignKey: "class_id"});
  student_classes.belongsTo(classes, { foreignKey: "class_id"});
  classes.hasMany(student_classes, { foreignKey: "class_id"});
  competency_grades.belongsTo(competencies, { foreignKey: "competency_id"});
  competencies.hasMany(competency_grades, { foreignKey: "competency_id"});
  subject_scores.belongsTo(competencies, { foreignKey: "competency_id"});
  competencies.hasMany(subject_scores, { foreignKey: "competency_id"});
  competencies.belongsTo(competency_categories, { foreignKey: "compentency_category_id"});
  competency_categories.hasMany(competencies, { foreignKey: "compentency_category_id"});
  competency_grades.belongsTo(grades, { foreignKey: "grade_id"});
  grades.hasMany(competency_grades, { foreignKey: "grade_id"});
  user_roles.belongsTo(roles, { foreignKey: "code"});
  roles.hasMany(user_roles, { foreignKey: "code"});
  school_bill_payments.belongsTo(school_bills, { foreignKey: "school_bill_id"});
  school_bills.hasMany(school_bill_payments, { foreignKey: "school_bill_id"});
  classes.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(classes, { foreignKey: "period_id"});
  school_attendances.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(school_attendances, { foreignKey: "period_id"});
  school_attendances_summaries.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(school_attendances_summaries, { foreignKey: "period_id"});
  school_bills.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(school_bills, { foreignKey: "period_id"});
  subjects.belongsTo(subject_categories, { foreignKey: "category_id"});
  subject_categories.hasMany(subjects, { foreignKey: "category_id"});
  class_subjects.belongsTo(subjects, { foreignKey: "subject_id"});
  subjects.hasMany(class_subjects, { foreignKey: "subject_id"});
  competencies.belongsTo(subjects, { foreignKey: "subject_id"});
  subjects.hasMany(competencies, { foreignKey: "subject_id"});
  class_attendances.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(class_attendances, { foreignKey: "student_id"});
  class_sessions.belongsTo(users, { foreignKey: "teacher_id"});
  users.hasMany(class_sessions, { foreignKey: "teacher_id"});
  class_subjects.belongsTo(users, { foreignKey: "teacher_id"});
  users.hasMany(class_subjects, { foreignKey: "teacher_id"});
  classes.belongsTo(users, { foreignKey: "teacher_id"});
  users.hasMany(classes, { foreignKey: "teacher_id"});
  histories.belongsTo(users, { foreignKey: "created_by"});
  users.hasMany(histories, { foreignKey: "created_by"});
  school_attendances.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(school_attendances, { foreignKey: "user_id"});
  school_attendances_summaries.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(school_attendances_summaries, { foreignKey: "user_id"});
  school_bill_payments.belongsTo(users, { foreignKey: "created_by"});
  users.hasMany(school_bill_payments, { foreignKey: "created_by"});
  school_bills.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(school_bills, { foreignKey: "student_id"});
  school_periods.belongsTo(users, { foreignKey: "principal_id"});
  users.hasMany(school_periods, { foreignKey: "principal_id"});
  student_classes.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(student_classes, { foreignKey: "student_id"});
  student_subjects.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(student_subjects, { foreignKey: "student_id"});
  subject_scores.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(subject_scores, { foreignKey: "student_id"});
  user_details.belongsTo(users, { foreignKey: "user_id"});
  users.hasOne(user_details, { foreignKey: "user_id"});
  user_roles.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(user_roles, { foreignKey: "user_id"});

  return {
    cities,
    class_attendances,
    class_majors,
    class_schedules,
    class_sessions,
    class_subjects,
    classes,
    competencies,
    competency_categories,
    competency_grades,
    grades,
    histories,
    roles,
    school_attendances,
    school_attendances_summaries,
    school_bill_payments,
    school_bills,
    school_periods,
    settings,
    student_classes,
    student_subjects,
    subject_categories,
    subject_scores,
    subjects,
    user_details,
    user_roles,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
