const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('student_subjects', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    class_subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'class_subjects',
        key: 'id'
      }
    },
    subject_name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      comment: "filled in after the school period is over"
    },
    student_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    final_score: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "filled in after the school period is over"
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "filled in after the school period is over"
    }
  }, {
    sequelize,
    tableName: 'student_subjects',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "student_subjects_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "class_subject_id" },
          { name: "student_id" },
        ]
      },
      {
        name: "student_subjects_class_subject_id",
        using: "BTREE",
        fields: [
          { name: "class_subject_id" },
        ]
      },
      {
        name: "student_subjects_student_id",
        using: "BTREE",
        fields: [
          { name: "student_id" },
        ]
      },
    ]
  });
};
