import Joi from "joi";
import { histories } from "../models";

class History {
  static async createHistory(historyData) {
    const schema = Joi.array()
      .items(
        Joi.object()
          .keys({
            action: Joi.string().trim().required(),
            table_name: Joi.string().max(100).trim().required(),
            table_id: Joi.any().optional(),
            created_by: Joi.number().required(),
            previous_data: Joi.any().optional(),
          })
          .required()
      )
      .required();

    const validate = schema.validate(historyData);

    if (validate.error) {
      throw new Error("Invalid history data, " + validate.error.message);
    }

    await histories.bulkCreate(historyData);
  }
}

module.exports = History;
