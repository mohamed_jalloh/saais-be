import { createClient } from 'redis';

const redisClient = createClient();
redisClient.connect();

module.exports = redisClient
