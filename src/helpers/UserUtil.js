import JWT from "jsonwebtoken";
import MD5 from "md5";
import { Sequelize } from "sequelize";
import RedisClient from "../helpers/Redis";
import {
  cities,
  classes,
  class_majors,
  roles,
  school_periods,
  student_classes,
  users,
  user_details,
  user_roles,
} from "../models";

class UserUtil {
  static async getUserData(username, id) {
    let user;

    if (!username && !id) {
      return user;
    }

    if (id) {
      user = await RedisClient.get("SaaisUserData:" + id);
      if (user) {
        user = JSON.parse(user);
      }
    }

    if (!user) {
      let whereQuery = {};

      if (username) {
        whereQuery.username = username;
      }

      if (id) {
        whereQuery.id = id;
      }

      user = await users.findOne({
        where: whereQuery,
        attributes: {
          exclude: ["created_at", "updated_at", "deleted_at"],
        },
        include: [
          {
            model: user_details,
            required: false,
            attributes: {
              exclude: ["created_at", "updated_at"],
              include: [
                [Sequelize.literal("`user_detail->city`.name"), "city_name"],
                [
                  Sequelize.literal("`user_detail->city`.province_name"),
                  "city_province_name",
                ],
              ],
            },
            include: {
              model: cities,
              required: false,
              attributes: [],
            },
          },
          {
            model: user_roles,
            required: false,
            separate: true,
            attributes: ["code", [Sequelize.col("role.name"), "name"]],
            include: {
              model: roles,
              attributes: [],
            },
          },
          {
            model: student_classes,
            required: false,
            separate: true,
            attributes: [
              "class_id",
              [Sequelize.col("class.school_period.year"), "period_year"],
              [
                Sequelize.col("class.school_period.semester"),
                "period_semester",
              ],
              [Sequelize.col("class.class_major.name"), "major_name"],
              [Sequelize.col("class.grade"), "grade"],
            ],
            include: {
              model: classes,
              attributes: [],
              include: [
                {
                  model: class_majors,
                  attributes: [],
                },
                {
                  model: school_periods,
                  attributes: [],
                },
              ],
            },
          },
        ],
      });

      if (user) {
        user = user.toJSON();
      } else {
        return;
      }

      user.hash = MD5(JSON.stringify(user));

      let userWithoutPass = { ...user };
      delete userWithoutPass.password;

      await RedisClient.set(
        "SaaisUserData:" + user.id,
        JSON.stringify(userWithoutPass)
      );
      await RedisClient.expire("SaaisUserData:" + user.id, 60 * 60 * 24);
    }

    return user;
  }

  static async getToken(user) {
    let token = JWT.sign(
      {
        id: user.id,
        username: user.username,
        user_roles: user.user_roles,
      },
      process.env.JWT_SECRET_KEY,
      {
        algorithm: "HS256",
        jwtid: user.hash,
      }
    );

    return token;
  }
}

module.exports = UserUtil;
