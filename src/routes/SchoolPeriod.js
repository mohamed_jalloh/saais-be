import { Router } from "express";
import { create, list, update } from "../controllers/SchoolPeriodController";

const routes = Router();

routes.put("/period/:id", update);
routes.post("/period", create);
routes.get("/period", list);

module.exports = routes;
