import { Router } from "express";
import {
  create,
  createClassSubject,
  list,
  removeClassSubject,
  update,
  updateStudent,
} from "../controllers/ClassController";

const routes = Router();

routes.put("/class/:id", update);
routes.put("/class/:id/student", updateStudent);
routes.put("/class/:id/add-subject", createClassSubject);
routes.put("/class/:id/remove-subject", removeClassSubject);
routes.post("/class", create);
routes.get("/class", list);

module.exports = routes;
