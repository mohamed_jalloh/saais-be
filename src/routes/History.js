import { Router } from "express";
import { del, list } from "../controllers/HistoryController";

const routes = Router();

routes.get("/history", list);
routes.delete("/history", del);

module.exports = routes;
