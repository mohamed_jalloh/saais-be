import { Router } from "express";
import { generate, list } from "../controllers/SchoolBillController";

const routes = Router();

routes.post("/school-bill", generate);
routes.get("/school-bill", list);

module.exports = routes;
