import { Router } from "express";
import { create, list, update } from "../controllers/SubjectController";

const routes = Router();

routes.get("/subject", list);
routes.put("/subject/:id", update);
routes.post("/subject", create);

module.exports = routes;
