import { Router } from "express";
import { list } from "../controllers/SubjectCategoryController";

const routes = Router();

routes.get("/subject-category", list);

module.exports = routes;
