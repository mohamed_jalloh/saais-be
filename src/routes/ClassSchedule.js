import { Router } from "express";
import { create, list, update } from "../controllers/ClassScheduleController";

const routes = Router();

routes.put("/schedule/:id", update);
routes.post("/schedule", create);
routes.get("/schedule", list);

module.exports = routes;
