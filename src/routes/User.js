import { Router } from "express";
import { create, findOne, list, update } from "../controllers/UserController";

const routes = Router();

routes.get("/user/:id", findOne);
routes.put("/user/:id", update);
routes.post("/user", create);
routes.get("/user", list);

module.exports = routes;
