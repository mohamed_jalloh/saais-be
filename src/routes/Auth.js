import { Router } from "express";
import { login, logout } from "../controllers/AuthController";
import LoginCheck from "../middlewares/LoginCheck";

const routes = Router();

routes.post("/auth/login", login);
routes.delete("/auth/logout", LoginCheck, logout);

module.exports = routes;
