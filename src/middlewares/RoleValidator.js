import ResMsg from "../assets/string/ResponseMessage.json";
import Response from "../helpers/Response";

export default function roleValidator(req, res, next, role) {
  if (role.includes(req.auth.role) > 0) {
    next();
  } else {
    return res
      .status(ResMsg.auth.invalidAccess.code)
      .json(Response.error(ResMsg.auth.invalidAccess.message));
  }
}
