import Joi from "joi";
import Sequelize, { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  class_majors,
  class_schedules,
  class_subjects,
  db,
  subjects,
  users,
} from "../models";

class ClassScheduleController {
  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          include: [
            [Sequelize.col("class_subject->class.name"), "class_name"],
            [
              Sequelize.col("class_subject->class->class_major.name"),
              "major_name",
            ],
            [Sequelize.col("class_subject->class.grade"), "class_grade"],
            [Sequelize.col("class_subject->class->user.name"), "teacher_name"],
            [Sequelize.col("class_subject->subject.name"), "subject_name"],
          ],
        },
        include: {
          model: class_subjects,
          attributes: [],
          include: [
            {
              model: subjects,
              attributes: [],
            },
            {
              model: classes,
              attributes: [],
              where: {
                period_id: req.query.period_id,
              },
              include: [
                {
                  model: users,
                  attributes: [],
                },
                {
                  model: class_majors,
                  attributes: [],
                },
              ],
            },
          ],
        },
        order: [
          ["day", "asc"],
          ["start_time", "asc"],
        ],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await class_schedules.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "ClassSchedule:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = Joi.object().keys({
        class_subject_id: Joi.number().required(),
        day: Joi.number().min(1).max(7).required(),
        start_time: Joi.string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
        finish_time: Joi.string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // create schedule
        let schedule = await class_schedules.create(req.body);

        // create history
        await createHistory([
          {
            action: "ClassSchedule:create",
            table_name: "class_schedules.class_subject_id",
            table_id: schedule.class_subject_id,
            created_by: req.auth.id,
            previous_data: schedule.toJSON(),
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSchedule:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = Joi.object().keys({
        day: Joi.number().min(1).max(7).required(),
        start_time: Joi.string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
        finish_time: Joi.string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        let schedule = await class_schedules.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!schedule) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        let prevData = schedule.toJSON();

        // update schedule
        await schedule.update(req.body);

        // create history
        await createHistory([
          {
            action: "ClassSchedule:update",
            table_name: "class_schedules.class_subject_id",
            table_id: schedule.class_subject_id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSchedule:update" });
    }
  }
}

module.exports = ClassScheduleController;
