import Joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import { db, subjects } from "../models";

class SubjectController {
  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);
      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          exclude: ["created_at", "updated_at", "deleted_at"],
        },
        order: [["name", "asc"]],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await subjects.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Subject:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = Joi.object().keys({
        major_id: Joi.number().required(),
        category_id: Joi.number().required(),
        name: Joi.string().min(4).max(100).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // create subject
        let subject = await subjects.create(req.body);

        // create history
        await createHistory([
          {
            action: "Subject:create",
            table_name: "subjects",
            table_id: subject.id,
            created_by: req.auth.id,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Subject:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = Joi.object().keys({
        major_id: Joi.number().required(),
        category_id: Joi.number().required(),
        name: Joi.string().min(4).max(100).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        let subject = await subjects.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!subject) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        let prevData = subject.toJSON();

        // update subject
        await subject.update(req.body);

        // create history
        await createHistory([
          {
            action: "Subject:update",
            table_name: "subjects",
            table_id: subject.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Subject:update" });
    }
  }
}

module.exports = SubjectController;
