import Joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import RedisClient from "../helpers/Redis";
import Response from "../helpers/Response";
import { db, school_periods, users, user_roles } from "../models";

class SchoolPeriodController {
  static async create(req, res, next) {
    try {
      const schema = Joi.object().keys({
        year: Joi.number().min(999).max(9999).required(),
        semester: Joi.string().valid("odd", "even").required(),
        principal_id: Joi.number().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // validate principal
        let principal = await users.findOne({
          where: {
            id: req.body.principal_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!principal) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }

        // create period
        let period = await school_periods.create(req.body);

        // create history
        await createHistory([
          {
            action: "SchoolPeriod:create",
            table_name: "school_periods",
            table_id: period.id,
            created_by: req.auth.id,
            previous_data: period.toJSON(),
          },
        ]);

        // delete period in redis
        await RedisClient.del("SaaisSetting:SchoolPeriod");

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolPeriod:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = Joi.object().keys({
        year: Joi.number().min(999).max(9999).required(),
        semester: Joi.string().valid("odd", "even").required(),
        principal_id: Joi.number().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // find existing period
        let period = await school_periods.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!period) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // validate principal
        let principal = await users.findOne({
          where: {
            id: req.body.principal_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!principal) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }

        let prevData = period.toJSON();

        // update period
        await period.update(req.body);

        // create history
        await createHistory([
          {
            action: "SchoolPeriod:update",
            table_name: "school_periods",
            table_id: period.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolPeriod:update" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);
      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          exclude: ["created_at", "updated_at", "deleted_at"],
        },
        order: [
          ["year", "desc"],
          ["created_at", "desc"],
        ],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await school_periods.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "SchoolPeriod:list" });
    }
  }
}

module.exports = SchoolPeriodController;
