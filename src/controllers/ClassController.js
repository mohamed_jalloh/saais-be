import Joi from "joi";
import _ from "lodash";
import { Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  class_majors,
  class_subjects,
  db,
  school_periods,
  student_classes,
  subjects,
  users,
  user_roles,
} from "../models";

class ClassController {
  static async create(req, res, next) {
    try {
      const schema = Joi.object().keys({
        name: Joi.string().max(100).trim().required(),
        major_id: Joi.number().required(),
        teacher_id: Joi.number().required(),
        grade: Joi.string().valid("x", "xi", "xii").required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        // validate teacher
        let teacher = await users.findOne({
          where: {
            id: req.body.teacher_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!teacher) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }

        // create class
        let schoolClass = await classes.create(req.body);

        // create history
        await createHistory([
          {
            action: "Class:create",
            table_name: "classes",
            table_id: schoolClass.id,
            created_by: req.auth.id,
            previous_data: schoolClass.toJSON(),
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = Joi.object().keys({
        name: Joi.string().max(100).trim().required(),
        major_id: Joi.number().required(),
        teacher_id: Joi.number().required(),
        grade: Joi.string().valid("x", "xi", "xii").required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        // find existing class
        let schoolClass = await classes.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!schoolClass) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // validate teacher
        let teacher = await users.findOne({
          where: {
            id: req.body.teacher_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!teacher) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }

        let prevData = schoolClass.toJSON();

        // update class
        await schoolClass.update(req.body);

        // create history
        await createHistory([
          {
            action: "Class:update",
            table_name: "classes",
            table_id: schoolClass.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:update" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          include: [
            [Sequelize.col("user.name"), "teacher_name"],
            [Sequelize.col("class_major.name"), "major_name"],
            [Sequelize.col("school_period.year"), "period_year"],
            [Sequelize.col("school_period.semester"), "period_semester"],
          ],
        },
        where: {
          period_id: req.query.period_id,
        },
        include: [
          {
            model: class_majors,
            attributes: [],
          },
          {
            model: school_periods,
            attributes: [],
          },
          {
            model: student_classes,
            required: false,
            separate: true,
            attributes: ["student_id", [Sequelize.col("user.name"), "name"]],
            include: {
              model: users,
              attributes: [],
            },
          },
          {
            model: class_subjects,
            required: false,
            separate: true,
            attributes: [
              "id",
              "subject_id",
              [Sequelize.col("subject.name"), "name"],
            ],
            include: {
              model: subjects,
              attributes: [],
            },
          },
          {
            model: users,
            attributes: [],
          },
        ],
        order: [
          ["period_id", "desc"],
          ["major_id", "asc"],
          ["grade", "asc"],
          ["name", "asc"],
        ],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await classes.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Class:list" });
    }
  }

  static async updateStudent(req, res, next) {
    try {
      const schema = Joi.object().keys({
        student_ids: Joi.array().items(Joi.number().required()).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate student
      let students = await users.findAll({
        where: {
          id: req.body.student_ids,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "student",
          },
        },
      });

      if (students.length != req.body.student_ids.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      // map user input
      let studentClasses = req.body.student_ids.map((item) => {
        return {
          student_id: item,
          class_id: req.params.id,
        };
      });

      await db.transaction(async (dbTrx) => {
        // get current student class
        let prevData = await student_classes.findAll({
          where: {
            class_id: req.params.id,
          },
          lock: true,
        });

        // create history
        await createHistory([
          {
            action: "Class:updateStudent",
            table_name: "student_classes.class_id",
            table_id: req.params.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        // destroy current student class
        await student_classes.destroy({
          where: {
            class_id: req.params.id,
          },
        });

        // create new student class
        await student_classes.bulkCreate(studentClasses);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:updateStudent" });
    }
  }

  static async createClassSubject(req, res, next) {
    try {
      const schema = Joi.object().keys({
        subjects: Joi.array()
          .items(
            Joi.object()
              .keys({
                teacher_id: Joi.number().required(),
                subject_id: Joi.number().required(),
              })
              .required()
          )
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      let teacher_ids = [];
      for await (let subject of req.body.subjects) {
        teacher_ids.push(subject.teacher_id);

        subject.class_id = req.params.id;
      }

      teacher_ids = _.uniq(teacher_ids);

      // validate student
      let teachers = await users.findAll({
        where: {
          id: teacher_ids,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "teacher",
          },
        },
      });

      if (teachers.length != teacher_ids.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      await db.transaction(async (dbTrx) => {
        // create new class subject
        await class_subjects.bulkCreate(req.body.subjects);

        // create history
        await createHistory([
          {
            action: "Class:createClassSubject",
            table_name: "class_subjects.class_id",
            table_id: req.params.id,
            created_by: req.auth.id,
            previous_data: req.body.subjects,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:createClassSubject" });
    }
  }

  static async removeClassSubject(req, res, next) {
    try {
      const schema = Joi.object().keys({
        subject_ids: Joi.array().items(Joi.number().required()).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // get current class subject
        let prevData = await class_subjects.findAll({
          where: {
            class_id: req.params.id,
            subject_id: req.body.subject_ids,
          },
          lock: true,
        });

        // destroy current class subject
        await class_subjects.destroy({
          where: {
            class_id: req.params.id,
            subject_id: req.body.subject_ids,
          },
        });

        // create history
        await createHistory([
          {
            action: "Class:updateSubject",
            table_name: "class_subjects.class_id",
            table_id: req.params.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:removeClassSubject" });
    }
  }
}

module.exports = ClassController;
