import Joi from "joi";
import moment from "moment";
import { Op, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  db,
  school_attendances,
  student_classes,
  users,
  user_roles,
} from "../models";

class SchoolAttendanceController {
  static async clockInOut(req, res, next) {
    // realtime clock in/out
    try {
      const schema = Joi.object().keys({
        username: Joi.string().min(4).max(100).trim().required(),
        attendance_type: Joi.string().valid("clock_in", "clock_out").required(),
        pic_url: Joi.string().uri().optional().allow(""),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate user
      let user = await users.findOne({
        where: {
          username: req.body.username,
        },
      });

      if (!user) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      req.body = {
        ...req.body,
        user_id: user.id,
        period_id: req.setting.period_id,
        clock_in_date: moment().format("YYYY-MM-DD"),
        type: "h",
        is_late: false, // update this logic
      };

      let attendance = await school_attendances.findOne({
        where: {
          period_id: req.body.period_id,
          user_id: req.body.user_id,
          clock_in_date: req.body.clock_in_date,
        },
      });

      // set history data
      let prevData;
      if (attendance) {
        prevData = {
          ...attendance,
        };
      }

      // validate duplicate clock in
      if (attendance && req.body.attendance_type == "clock_in") {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }

      // validate clock out before clock in
      if (!attendance && req.body.attendance_type == "clock_out") {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      // vavlidate duplicate clock out
      if (attendance?.clock_out_at && req.body.attendance_type == "clock_out") {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }

      if (req.body.attendance_type == "clock_in") {
        req.body.clock_in_at = new Date();
        req.body.clock_in_pic =
          req.body.pic_url != "" ? req.body.pic_url : null;
      } else if (req.body.attendance_type == "clock_out") {
        req.body.clock_out_at = new Date();
        req.body.clock_out_pic =
          req.body.pic_url != "" ? req.body.pic_url : null;
      }

      await db.transaction(async (dbTrx) => {
        // create or update attendance
        if (attendance) {
          attendance = await attendance.update(req.body);
        } else {
          attendance = await school_attendances.create(req.body);
        }

        // create history
        await createHistory([
          {
            action: "SchoolAttendance:clockInOut",
            table_name: "school_attendences.user_id",
            table_id: user.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolAttendance:clockInOut" });
    }
  }

  static async manualCreate(req, res, next) {
    // manual create attendance from admin
    try {
      const schema = Joi.object().keys({
        user_id: Joi.number().required(),
        type: Joi.string().valid("h", "a", "i", "s").required(),
        clock_in_at: Joi.string().isoDate().optional(),
        clock_out_at: Joi.string().isoDate().optional(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate user
      let user = await users.findOne({
        where: {
          id: req.body.user_id,
        },
      });

      if (!user) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      req.body = {
        ...req.body,
        period_id: req.setting.period_id,
        clock_in_date: moment(req.body.clock_in_at).format("YYYY-MM-DD"),
        is_late: false, // update this logic
      };

      let attendance = await school_attendances.findOne({
        where: {
          period_id: req.body.period_id,
          user_id: req.body.user_id,
          clock_in_date: req.body.clock_in_date,
        },
      });

      // set history data
      let prevData;
      if (attendance) {
        prevData = attendance.toJSON();
      }

      if (req.body.type != "h") {
        req.body.clock_in_at = null;
        req.body.clock_out_at = null;
      }

      await db.transaction(async (dbTrx) => {
        // create or update attendance
        if (attendance) {
          await attendance.update(req.body);
        } else {
          await school_attendances.create(req.body);
        }

        // create history
        await createHistory([
          {
            action: "SchoolAttendance:manualCreate",
            table_name: "school_attendences.user_id",
            table_id: req.body.user_id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolAttendance:manualCreate" });
    }
  }

  static async createByClass(req, res, next) {
    // bulk create attendance for a class
    try {
      const schema = Joi.object().keys({
        class_id: Joi.number().required(),
        clock_in_date: Joi.string().isoDate().required(),
        absent_students: Joi.array()
          .items(
            Joi.object().keys({
              user_id: Joi.number().required(),
              type: Joi.string().valid("a", "i", "s").required(),
              clock_in_pic: Joi.string().optional().allow(""),
            })
          )
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      let attendanceData = [];

      let absentStudentIds = await req.body.absent_students.map((item) => {
        // generate absent student data
        attendanceData.push({
          ...item,
          period_id: req.body.period_id,
          clock_in_date: moment(req.body.clock_in_date).format("YYYY-MM-DD"),
          clock_in_at: null,
          clock_out_at: null,
          clock_out_pic: null,
          clock_in_pic: item.clock_in_pic ?? null,
          is_late: false, // update this logic
        });

        return item.user_id;
      });

      // validate absent student
      let absentStudents = await users.findAll({
        where: {
          id: absentStudentIds,
        },
        include: [
          {
            model: user_roles,
            required: true,
            attributes: [],
            where: {
              code: "student",
            },
          },
          {
            model: student_classes,
            attributes: [],
            required: true,
            include: {
              model: classes,
              attributes: [],
              required: true,
              where: {
                period_id: req.body.period_id,
                id: req.body.class_id,
              },
            },
          },
        ],
      });

      if (absentStudents.length != req.body.absent_students.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      // generate attend student data
      let students = await users.findAll({
        where: {
          id: {
            [Op.notIn]: absentStudentIds,
          },
        },
        attributes: ["id"],
        include: [
          {
            model: user_roles,
            required: true,
            attributes: [],
            where: {
              code: "student",
            },
          },
          {
            model: student_classes,
            attributes: [],
            required: true,
            include: {
              model: classes,
              attributes: [],
              required: true,
              where: {
                period_id: req.body.period_id,
                id: req.body.class_id,
              },
            },
          },
        ],
      });

      for await (let student of students) {
        attendanceData.push({
          period_id: req.body.period_id,
          clock_in_date: moment(req.body.clock_in_date).format("YYYY-MM-DD"),
          clock_in_at: null,
          clock_out_at: null,
          clock_out_pic: null,
          clock_in_pic: null,
          is_late: false, // update this logic
          type: "h",
          user_id: student.id,
        });
      }

      await db.transaction(async (dbTrx) => {
        // bulk create attendance
        await school_attendances.bulkCreate(attendanceData);

        // create history
        await createHistory([
          {
            action: "SchoolAttendance:createByClass",
            table_name: "school_attendences.class_id",
            table_id: req.body.class_id,
            created_by: req.auth.id,
            previous_data: attendanceData,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolAttendance:createByClass" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        order: [["created_at", "desc"]],
        where: {
          period_id: req.query.period_id,
        },
        attributes: {
          exclude: ["created_at", "updated_at"],
        },
        include: [
          {
            model: users,
            attributes: ["name", "username"],
          },
        ],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await school_attendances.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "SchoolAttendance:list" });
    }
  }
}

module.exports = SchoolAttendanceController;
