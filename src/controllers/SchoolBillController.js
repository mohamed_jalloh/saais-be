import Joi from "joi";
import { Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  db,
  school_bills,
  school_bill_payments,
  student_classes,
  users,
  user_roles,
} from "../models";

class SchoolBillController {
  static async changeBillStatus(billId, periodId, studentIds, amount) {
    let billQuery = {
      where: {
        student_id: studentIds,
        period_id: periodId,
      },
      attributes: [
        "id",
        "amount",
        "status",
        [
          Sequelize.literal("sum(school_bill_payments.amount)"),
          "total_payment",
        ],
      ],
      include: {
        model: school_bill_payments,
        attributes: [],
      },
      group: ["school_bills.id"],
      lock: true,
    };

    if (billId) {
      billQuery.where = {
        ...billQuery.where,
        id: billId,
      };
    }

    let bills = await school_bills.findAll(billQuery);

    for await (let bill of bills) {
      if (!amount) {
        amount = Number(bill.amount);
      }

      let status = "due";
      if (
        amount - Number(bill.dataValues.total_payment) > 0 &&
        Number(bill.dataValues.total_payment) != 0
      ) {
        status = "partial";
      } else if (amount - Number(bill.dataValues.total_payment) <= 0) {
        status = "paid";
      }

      await school_bills.update(
        {
          status,
        },
        { where: { id: bill.id } }
      );
    }

    return;
  }

  static async generate(req, res, next) {
    try {
      const schema = Joi.object().keys({
        amount: Joi.number().required(),
        class_grade: Joi.string().valid("x", "xi", "xii").required(),
        force_regenerate: Joi.boolean().required(),
        student_ids: Joi.array().items(Joi.number().optional()).optional(),
        note: Joi.string().optional().allow(""),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        // generate bill data
        let studentsQuery = {
          include: [
            {
              model: user_roles,
              required: true,
              attributes: [],
              where: {
                code: "student",
              },
            },
            {
              model: student_classes,
              required: true,
              attributes: [],
              include: {
                model: classes,
                required: true,
                attributes: [],
                where: {
                  period_id: req.body.period_id,
                  grade: req.body.class_grade,
                },
              },
            },
          ],
        };

        if (req.query.student_ids?.length > 0) {
          studentsQuery.where = {
            ...studentsQuery.where,
            id: req.query.student_ids,
          };
        }

        let students = await users.findAll(studentsQuery);

        let months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        let studentIds = await students.map((item) => {
          return item.id;
        });

        let bills = [];

        for await (let month of months) {
          for await (let student of students) {
            bills.push({
              ...req.body,
              student_id: student.id,
              month,
            });
          }
        }

        // generate school bills
        if (req.body.force_regenerate) {
          // destroy genereated bills
          await school_bills.destroy({
            where: {
              student_id: studentIds,
              period_id: req.body.period_id,
            },
          });
        }

        await school_bills.bulkCreate(bills);

        await SchoolBillController.changeBillStatus(
          null,
          req.body.period_id,
          studentIds,
          req.body.amount
        );

        // create history
        await createHistory([
          {
            action: "SchoolBill:generate",
            table_name: "school_bills.period_id",
            table_id: req.body.period_id,
            created_by: req.auth.id,
            previous_data: bills,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolBill:generate" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        where: {
          period_id: req.query.period_id,
        },
      };

      if (req.query.student_id > 0) {
        query.where = {
          ...query.where,
          student_id: req.query.student_id,
        };
      }

      if (req.query.month > 0) {
        query.where = {
          ...query.where,
          month: req.query.month,
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await school_bills.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "SchoolBill:list" });
    }
  }
}

module.exports = SchoolBillController;
