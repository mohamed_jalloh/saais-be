import Bcrypt from "bcrypt";
import Joi from "joi";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import RedisClient from "../helpers/Redis";
import Response from "../helpers/Response";
import { getToken, getUserData } from "../helpers/UserUtil";

class AuthController {
  static async login(req, res, next) {
    try {
      const schema = Joi.object().keys({
        username: Joi.string().min(4).max(100).trim().required(),
        password: Joi.string().min(6).max(100).trim().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      //get user data
      let user = await getUserData(req.body.username);

      if (user && user.is_active) {
        // validate for reset password
        if (user.is_reset) {
          return res
            .status(ResMsg.auth.resetPasswordRequired.code)
            .json(Response.error(ResMsg.auth.resetPasswordRequired.message));
        }

        // validate password
        let isValid = await Bcrypt.compare(req.body.password, user.password);

        delete user.password;

        if (isValid) {
          // get token
          let token = await getToken(user);

          // create history
          await createHistory([
            {
              action: "Auth:login",
              table_name: "users",
              table_id: user.id,
              created_by: user.id,
            },
          ]);

          return res.json(Response.success({ token }));
        } else {
          return res
            .status(ResMsg.auth.invalidCredential.code)
            .json(Response.error(ResMsg.auth.invalidCredential.message));
        }
      } else {
        return res
          .status(ResMsg.auth.invalidCredential.code)
          .json(Response.error(ResMsg.auth.invalidCredential.message));
      }
    } catch (error) {
      next({ error, fun: "Auth:login" });
    }
  }

  static async logout(req, res, next) {
    try {
      // delete user date in redis
      await RedisClient.del("SaaisUserData:" + req.auth.id);
      return res.json(Response.success());
    } catch (error) {
      next({ error, fun: "Auth:logout" });
    }
  }
}

module.exports = AuthController;
