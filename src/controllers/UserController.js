import Bcrypt from "bcrypt";
import Crypto from "crypto";
import Joi from "joi";
import { Op, Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import { getUserData } from "../helpers/UserUtil";
import {
  cities,
  classes,
  class_majors,
  db,
  roles,
  school_periods,
  student_classes,
  users,
  user_details,
  user_roles,
} from "../models";

class UserController {
  static async findOne(req, res, next) {
    try {
      // get user data
      let user = await getUserData(null, req.params.id);

      delete user.password;

      if (!user) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(user));
    } catch (error) {
      next({ error, fun: "User:profile" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = Joi.object().keys({
        username: Joi.string().min(4).max(100).trim().required(),
        name: Joi.string().min(4).max(100).trim().required(),
        user_roles: Joi.array()
          .items(
            Joi.object()
              .keys({
                code: Joi.string().required(),
              })
              .required()
          )
          .optional(),
        user_details: Joi.object()
          .keys({
            nisn: Joi.number().optional().allow(""),
            nik: Joi.number().optional().allow(""),
            address: Joi.string().trim().optional().allow(""),
            place_of_birth_id: Joi.number().optional().allow(""),
            date_of_birth: Joi.string().isoDate().optional().allow(""),
            gender: Joi.string().valid("male", "female").optional().allow(""),
            religion: Joi.string().max(100).trim().optional().allow(""),
            education: Joi.string().max(100).trim().optional().allow(""),
            phone_num: Joi.string().max(100).trim().optional().allow(""),
            status_in_family: Joi.string().max(100).trim().optional().allow(""),
            order_in_family: Joi.number().optional().allow(""),
            origin_school: Joi.string().max(100).trim().optional().allow(""),
            join_date: Joi.string().isoDate().optional().allow(""),
            join_class: Joi.string()
              .valid("x", "xi", "xii")
              .optional()
              .allow(""),
            father_name: Joi.string().max(100).trim().optional().allow(""),
            father_occupation: Joi.string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            mother_name: Joi.string().max(100).trim().optional().allow(""),
            mother_occupation: Joi.string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            parent_phone_num: Joi.string().max(100).trim().optional().allow(""),
            parent_address: Joi.string().trim().optional().allow(""),
            alumni_education: Joi.string().max(100).trim().optional().allow(""),
            alumni_job: Joi.string().max(100).trim().optional().allow(""),
            graduated_at: Joi.string().isoDate().optional().allow(""),
          })
          .optional(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // remove empty field
        for (let key in req.body) {
          if (!req.body[key]) {
            delete req.body[key];
          }
        }

        // create and encrypt password
        const salt = await Bcrypt.genSalt(12);
        req.body.password = await Bcrypt.hash(
          Crypto.randomBytes(32).toString("hex"),
          salt
        );

        // create user
        let user = await users.create(req.body);

        let historyData = [
          {
            action: "User:create",
            table_name: "users",
            table_id: user.id,
            created_by: req.auth.id,
          },
        ];

        // remove old user roles
        await user_roles.destroy({
          where: {
            user_id: user.id,
          },
          lock: true,
        });

        for await (let role of req.body.user_roles) {
          role.user_id = user.id;
        }

        // create new user roles
        await user_roles.bulkCreate(req.body.user_roles);

        historyData.push({
          action: "User:create",
          table_name: "user_roles.user_id",
          table_id: user.id,
          created_by: req.auth.id,
          previous_data: req.body.roles,
        });

        // create or update user detail if exist
        if (req.body.user_details && req.body.user_details != {}) {
          // delete empty field
          for (let key in req.body.user_details) {
            if (!req.body.user_details[key]) {
              delete req.body.user_details[key];
            }
          }

          let userDetails = await user_details.create({
            ...req.body.user_details,
            user_id: user.id,
          });

          historyData.push({
            action: "User:create",
            table_name: "user_details.user_id",
            table_id: user.id,
            created_by: req.auth.id,
            previous_data: userDetails.toJSON(),
          });
        }

        await createHistory(historyData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.user.duplicateUsername.code)
          .json(Response.error(ResMsg.user.duplicateUsername.message));
      }
      next({ error, fun: "User:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = Joi.object().keys({
        username: Joi.string().min(4).max(100).trim().required(),
        name: Joi.string().min(4).max(100).trim().required(),
        password: Joi.string().min(6).max(100).trim().optional().allow(""),
        old_password: Joi.string().min(6).max(100).trim().optional().allow(""),
        is_active: Joi.boolean().optional().allow(""),
        is_reset: Joi.boolean().optional().allow(""),
        user_roles: Joi.array()
          .items(
            Joi.object()
              .keys({
                code: Joi.string().required(),
              })
              .optional()
          )
          .optional(),
        user_details: Joi.object()
          .keys({
            nisn: Joi.number().optional().allow(""),
            nik: Joi.number().optional().allow(""),
            address: Joi.string().trim().optional().allow(""),
            place_of_birth_id: Joi.number().optional().allow(""),
            date_of_birth: Joi.string().isoDate().optional().allow(""),
            gender: Joi.string().valid("male", "female").optional().allow(""),
            religion: Joi.string().max(100).trim().optional().allow(""),
            education: Joi.string().max(100).trim().optional().allow(""),
            phone_num: Joi.string().max(100).trim().optional().allow(""),
            status_in_family: Joi.string().max(100).trim().optional().allow(""),
            order_in_family: Joi.number().optional().allow(""),
            origin_school: Joi.string().max(100).trim().optional().allow(""),
            join_date: Joi.string().isoDate().optional().allow(""),
            join_class: Joi.string()
              .valid("x", "xi", "xii")
              .optional()
              .allow(""),
            father_name: Joi.string().max(100).trim().optional().allow(""),
            father_occupation: Joi.string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            mother_name: Joi.string().max(100).trim().optional().allow(""),
            mother_occupation: Joi.string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            parent_phone_num: Joi.string().max(100).trim().optional().allow(""),
            parent_address: Joi.string().trim().optional().allow(""),
            alumni_education: Joi.string().max(100).trim().optional().allow(""),
            alumni_job: Joi.string().max(100).trim().optional().allow(""),
            graduated_at: Joi.string().isoDate().optional().allow(""),
          })
          .optional(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // find user in db
        let user = await users.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!user) {
          return res
            .status(ResMsg.user.notFound.code)
            .json(Response.error(ResMsg.user.notFound.message));
        }

        // remove empty field
        for (let key in req.body) {
          if (!req.body[key]) {
            delete req.body[key];
          }
        }

        // create and encrypt password
        if (req.body.password) {
          let isValid = await Bcrypt.compare(
            req.body.old_password ?? "",
            user.password
          );

          if (!isValid) {
            return res
              .status(ResMsg.auth.invalidPassword.code)
              .json(Response.error(ResMsg.auth.invalidPassword.message));
          }

          const salt = await Bcrypt.genSalt(12);
          req.body.password = await Bcrypt.hash(req.body.password, salt);
        }

        let prevData = user.toJSON();
        delete prevData.password;

        // update user
        await user.update(req.body);

        let historyData = [
          {
            action: "User:update",
            table_name: "users",
            table_id: user.id,
            created_by: req.auth.id,
            previous_data: prevData,
          },
        ];

        // delete redis data if user not active
        if (!user.is_active) {
          await RedisClient.del("SaaisUserData:" + user.id);
        }

        // create or update user roles if exist
        if (req.body.user_roles && req.body.user_roles.length > 0) {
          // remove old user roles
          await user_roles.destroy({
            where: {
              user_id: user.id,
            },
            lock: true,
          });

          for await (let role of req.body.user_roles) {
            role.user_id = user.id;
          }

          // create new user roles
          await user_roles.bulkCreate(req.body.user_roles);

          historyData.push({
            action: "User:update",
            table_name: "user_roles.user_id",
            table_id: user.id,
            created_by: req.auth.id,
            previous_data: req.body.user_roles,
          });
        }

        // create or update user detail if exist
        if (req.body.user_details && req.body.user_details != {}) {
          // delete empty field
          for (let key in req.body.user_details) {
            if (!req.body.user_details[key]) {
              delete req.body.user_details[key];
            }
          }

          let userDetails = await user_details.findOne({
            where: {
              user_id: user.id,
            },
          });

          let prevData;
          if (userDetails) {
            prevData = userDetails.toJSON();

            await userDetails.update(req.body);
          } else {
            await user_details.create({
              ...req.body.user_details,
              user_id: user.id,
            });
          }

          historyData.push({
            action: "User:update",
            table_name: "user_details.user_id",
            table_id: user.id,
            created_by: req.auth.id,
            previous_data: prevData,
          });
        }

        await createHistory(historyData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.user.duplicateUsername.code)
          .json(Response.error(ResMsg.user.duplicateUsername.message));
      }
      next({ error, fun: "User:update" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);
      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          exclude: ["password", "created_at", "updated_at", "deleted_at"],
        },
        include: [
          {
            model: user_details,
            required: false,
            attributes: {
              exclude: ["created_at", "updated_at"],
              include: [
                [Sequelize.literal("`user_detail->city`.name"), "city_name"],
                [
                  Sequelize.literal("`user_detail->city`.province_name"),
                  "city_province_name",
                ],
              ],
            },
            include: {
              model: cities,
              required: false,
              attributes: [],
            },
          },
          {
            model: user_roles,
            required: false,
            separate: true,
            attributes: ["code", [Sequelize.col("role.name"), "name"]],
            include: {
              model: roles,
              attributes: [],
            },
          },
          {
            model: student_classes,
            required: false,
            separate: true,
            attributes: [
              "class_id",
              [Sequelize.col("class.school_period.year"), "period_year"],
              [
                Sequelize.col("class.school_period.semester"),
                "period_semester",
              ],
              [Sequelize.col("class.class_major.name"), "major_name"],
              [Sequelize.col("class.grade"), "grade"],
              [Sequelize.col("class.name"), "name"],
            ],
            include: {
              model: classes,
              attributes: [],
              include: [
                {
                  model: class_majors,
                  attributes: [],
                },
                {
                  model: school_periods,
                  attributes: [],
                },
              ],
            },
          },
        ],
        order: [["name", "asc"]],
      };

      if (req.query.search) {
        query.where = {
          ...query.where,
          [Op.or]: {
            username: {
              [Op.like]: "%" + req.query.search + "%",
            },
            name: {
              [Op.like]: "%" + req.query.search + "%",
            },
            "$user_detail.nik$": {
              [Op.like]: "%" + req.query.search + "%",
            },
            "$user_detail.nisn$": {
              [Op.like]: "%" + req.query.search + "%",
            },
          },
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await users.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "User:list" });
    }
  }
}

module.exports = UserController;
