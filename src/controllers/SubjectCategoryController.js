import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import { subject_categories } from "../models";

class SubjectCategoryController {
  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);
      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        order: [["name", "asc"]],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await subject_categories.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "SubjectCategory:list" });
    }
  }
}

module.exports = SubjectCategoryController;
