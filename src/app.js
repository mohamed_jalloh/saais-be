import Cors from "cors";
import "dotenv/config";
import Express from "express";
import Morgan from "morgan";
import ResMsg from "./assets/string/ResponseMessage.json";
import Logger from "./helpers/Logger";
import Response from "./helpers/Response";
import LoginCheck from "./middlewares/LoginCheck";
import RoleValidator from "./middlewares/RoleValidator";
import SchoolPeriodCheck from "./middlewares/SchoolPeriodCheck";

const app = Express();
const port = process.env.PORT || 3000;

app.use(
  Cors(),
  Express.json({
    limit: "10mb",
  }),
  Express.urlencoded({
    limit: "10mb",
    extended: true,
  })
);

if (process.env.NODE_ENV !== "production") app.use(Morgan("dev"));

let roleCheck = (req, res, next) => {
  RoleValidator(req, res, next, ["owner"]);
};

app.use(require("./routes/Home"));
app.use(require("./routes/Test"));
app.use(require("./routes/Auth"));
app.use(require("./routes/City"));

app.use(LoginCheck, require("./routes/History"));
app.use(LoginCheck, require("./routes/User"));
app.use(LoginCheck, require("./routes/Role"));
app.use(LoginCheck, require("./routes/ClassMajor"));
app.use(LoginCheck, require("./routes/SchoolPeriod"));
app.use(LoginCheck, require("./routes/SubjectCategory"));
app.use(LoginCheck, require("./routes/Subject"));

app.use(LoginCheck, SchoolPeriodCheck, require("./routes/Class"));
app.use(LoginCheck, SchoolPeriodCheck, require("./routes/ClassSchedule"));
app.use(LoginCheck, SchoolPeriodCheck, require("./routes/SchoolAttendence"));
app.use(LoginCheck, SchoolPeriodCheck, require("./routes/SchoolBill"));

app.use(function (error, req, res, next) {
  if (error.name == "UnauthorizedError") {
    return res
      .status(ResMsg.auth.invalidToken.code)
      .json(Response.error(ResMsg.auth.invalidToken.message));
  } else if (process.env.NODE_ENV == "production") {
    Logger.error(error.error);
    Logger.info(
      JSON.stringify({
        fun: error.fun,
        user: req.user ?? {},
        body: req.body ?? {},
      })
    );
  } else {
    console.log(error);
  }

  return res
    .status(ResMsg.general.generalError.code)
    .json(Response.error(ResMsg.general.generalError.message));
});

app.use(function (req, res, next) {
  return res
    .status(ResMsg.general.pageNotFound.code)
    .json(Response.error(ResMsg.general.pageNotFound.message));
});

app.listen(port, () => {
  console.log(`Saais Backend Server started on port ${port}`);
});

export default app;
